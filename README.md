### Install
---
Run `composer install` first.

### Configuration
---
Populate `.env` file with correct value.

### Usage
---
`php src/index.php input/subscriptions_test_subset.csv`
