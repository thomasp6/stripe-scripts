<?php

use Stripe\StripeClient;
use Stripe\Exception\ApiErrorException;

require_once __DIR__ . '/../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();

$stripeSecretKey = $_ENV['STRIPE_SECRET_KEY'];
$stripe = new StripeClient($stripeSecretKey);

$inputFile = $argv[1];
$parent_dir = realpath(dirname(__FILE__) . '/..');

$outputFile = fopen($parent_dir . '/output/to_check.csv' , 'w+') or die("can't open output file");
$endFile = fopen($parent_dir . '/output/processed.csv', 'w+') or die("can't open endfile");;
$subscriptionIdList = file($inputFile, FILE_IGNORE_NEW_LINES);

$i = 1;

foreach($subscriptionIdList as $subscriptionId) {

    echo $i++, " / ", count($subscriptionIdList), " - ", $subscriptionId, "\n";
    $scheduleId = null;

    try {
        $schedule = $stripe->subscriptionSchedules->create([
            'from_subscription' => $subscriptionId,
        ]);
    } catch (ApiErrorException $e) {
        fwrite($outputFile, $subscriptionId . "\n");
        // Retry those who fail - second run
        //preg_match('/\`(.+)\`/', $e->getMessage(), $matches);
        //$scheduleId = $matches[1];
        continue; // comment out on second run
    }

    $itemList = [];
    $subscription = $stripe->subscriptions->retrieve($subscriptionId);

    foreach ($subscription->items as $item) {
        $itemValue = [
            'price' => $item->price->id,
            'tax_rates' => $item->tax_rates,
        ];

        if (!empty($item->quantity)) {
            $itemValue['quantity'] = $item->quantity;
        }

        $itemList[] = $itemValue;
    }

    // Retry those who fail - second run
    if ($scheduleId) {
        $schedule = $stripe->subscriptionSchedules->retrieve($scheduleId);
    }

    $stripe->subscriptionSchedules->update($schedule->id, [
        'phases' => [
            [
                'items' => $itemList,
                'start_date' => $schedule->current_phase["start_date"],
                'end_date' => strtotime('first day of next month'),
            ],
            [
                'items' => $itemList,
                'iterations' => 1,
                'billing_cycle_anchor' => 'phase_start',
            ],
        ],
    ]);

    fwrite($endFile, $subscriptionId . "\n");
}
